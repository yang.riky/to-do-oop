import { Todo } from "./Todo.js";

export class TodoController {

    todoList = new Map();

    constructor() {
        let todoListArray = JSON.parse(localStorage.getItem('todoList'));

        if (todoListArray) {

            for (let todo in todoListArray) {
                this.todoList.set(todoListArray[todo].id,
                    Object.assign(new Todo(), todoListArray[todo]));
            }
        }
    }

    #getNewId() {
        let newId = 1;

        if (this.todoList.size > 0) {
            newId = Math.max(...this.todoList.keys()) + 1;
        }

        return newId;
    }

    addTodo(todo) {
        todo.setId(this.#getNewId());
        this.todoList.set(todo.getId(), todo);
        this.#saveToLocalStorage();
    }

    getTodoList() {
        return this.todoList;
    }

    updateTodo(id, name) {
        if (name === undefined) {
            this.todoList.get(id).setStatus(!this.todoList.get(id).getStatus());
        } else {
            this.todoList.get(id).setName(name);
        }

        this.#saveToLocalStorage();
    }

    deleteTodo(id) {
        this.todoList.delete(id);
        this.#saveToLocalStorage();
    }

    #saveToLocalStorage() {
        let obj = Object.fromEntries(this.todoList);
        localStorage.setItem('todoList', JSON.stringify(obj));
    }
}