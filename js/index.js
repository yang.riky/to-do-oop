import { Todo } from "./Todo.js";
import { TodoController } from "./TodoController.js";

let todoController = new TodoController();

document.getElementById("txtTodo").addEventListener("keyup", (event) => {
    if (event.key === "Enter") {
        addTodo();
    }
});

function getCheckBox(todo) {
    let checkbox = "";

    checkbox = `
    <div class="input-group" id=${todo.id}>
        <div class="input-group-text">
            <input class="form-check-input mt-0" type="checkbox" value=""
                aria-label="Checkbox ${todo.name}" ${todo.status ? "checked" : ""} onclick="updateTodo(${todo.id})">
        </div>

        <input type="text" class="form-control" aria-label="Todo ${todo.name}" value="${todo.name}" onfocusout="updateTodo(${todo.id}, this.value)" onkeyup="updateTodoEnterKey(event, ${todo.id}, this.value)" id="todo-${todo.id}">
        <button class="btn btn-outline-secondary" type="button" onclick="deleteTodo(${todo.id})"><i class="bi-trash"></i></button>
    </div>
    `;

    return checkbox;
}

window.addTodo = function () {
    let txtTodo = document.getElementById("txtTodo");

    if (txtTodo.value) {
        let todo = new Todo(txtTodo.value);

        todoController.addTodo(todo);

        showTodoList();

        txtTodo.value = "";
    }
}

function showTodoList() {
    let checkBox = "";

    todoController.getTodoList().forEach(todo => {
        checkBox += getCheckBox(todo);
    });

    document.getElementById("todoList").innerHTML = checkBox;
}

window.updateTodoEnterKey = function (event, id, name) {
    if (event.key === "Enter") {
        updateTodo(id, name);
    }
};

window.updateTodo = function (id, name) {
    if (name != "") {
        todoController.updateTodo(id, name);
    } else {
        let oldName = todoController.getTodoList().get(id).getName();

        let txtTodo = document.getElementById("todo-" + id).value = oldName;
    }
};

window.deleteTodo = function (id) {
    todoController.deleteTodo(id);
    document.getElementById(id).remove();
};

showTodoList();